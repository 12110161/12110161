﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _12110161_Web.Models
{
    public class LichThiDau
    {
        public int ID { get; set; }
        public int Vong { get; set; }
        public String ChuNha { get; set; }
        public String TiLe { get; set; }
        public String Khach { get; set; }
    }
}