﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace _12110161_Web.Models
{
    public class TheLoai
    {
        public int ID { get; set; }
        [Required]

        public String TenTheLoai { get; set; }


        public virtual ICollection<BangXepHang> BangXepHangs { get; set; }
        public virtual ICollection<Post> Posts { get; set; }
    }
}