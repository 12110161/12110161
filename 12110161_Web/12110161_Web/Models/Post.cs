﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace _12110161_Web.Models
{
    public class Post
    {
        public int ID { get; set; }
        [Required]
        [StringLength(500, ErrorMessage = "Phải nhập 20--> 500 kí tự", MinimumLength = 6)]
        public string Title { get; set; }
        [Required]
        [StringLength(500, ErrorMessage = "Phải nhập 20--> 500 kí tự", MinimumLength = 4)]
        public string Sumary { get; set; }
        [Required]
        [StringLength(10000, ErrorMessage = "Tối thiểu 50 kí tự", MinimumLength = 50)]
        public string Body { get; set; }


        [DataType(DataType.DateTime)]
        public System.DateTime DayCreate { get; set; }


        public int UserProfileUserId { get; set; }
        public virtual UserProfile UserProfile { get; set; }

        public int TheLoaiID { get; set; }
        public virtual TheLoai TheLoai { get; set; }



        public virtual ICollection<Comment> Comments { get; set; }
        public virtual ICollection<Tag> Tags { get; set; }
    }
}