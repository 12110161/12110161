﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _12110161_Web.Models
{
    public class BangXepHang
    {
        public int ID { get; set; }
        public String TenDoiBong { get; set; }
        public int SoTran { get; set; }
        public int Thang { get; set; }
        public int Thua { get; set; }
        public int Hoa { get; set; }
        public int HieuSo { get; set; }
        public int Diem { get; set; }

        public int TheLoaiID { get; set; }
        public virtual TheLoai TheLoai { get; set; }
    }
}