﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace _12110161_Web.Models
{
    public class Tag
    {
        public int TagID { get; set; }
        [Required]

        [StringLength(100, ErrorMessage = " Phải nhập từ 3 --> 100 kí tự ", MinimumLength = 3)]
        public string Content { get; set; }

        public virtual ICollection<Post> Posts { get; set; }
    }
}