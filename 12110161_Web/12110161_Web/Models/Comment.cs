﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace _12110161_Web.Models
{
    public class Comment
    {
        public int ID { get; set; }
        [Required]

        [StringLength(500, ErrorMessage = "Tối thiểu 50 kí tự", MinimumLength = 4)]
        public string Body { get; set; }


        [DataType(DataType.DateTime)]
        public DateTime DayCreate { get; set; }

        public int LastTime
        {
            get
            {
                return (DateTime.Now - DayCreate).Minutes;
            }
        }

        public int PostID { get; set; }
        public virtual Post Post { get; set; }
    }
}