namespace _12110161_Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ten4 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BangXepHangs",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        TenDoiBong = c.String(),
                        SoTran = c.Int(nullable: false),
                        Thang = c.Int(nullable: false),
                        Thua = c.Int(nullable: false),
                        Hoa = c.Int(nullable: false),
                        HieuSo = c.Int(nullable: false),
                        Diem = c.Int(nullable: false),
                        TheLoaiID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.TheLoais", t => t.TheLoaiID, cascadeDelete: true)
                .Index(t => t.TheLoaiID);
            
            CreateTable(
                "dbo.LichThiDaus",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Vong = c.Int(nullable: false),
                        ChuNha = c.String(),
                        TiLe = c.String(),
                        Khach = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.BangXepHangs", new[] { "TheLoaiID" });
            DropForeignKey("dbo.BangXepHangs", "TheLoaiID", "dbo.TheLoais");
            DropTable("dbo.LichThiDaus");
            DropTable("dbo.BangXepHangs");
        }
    }
}
