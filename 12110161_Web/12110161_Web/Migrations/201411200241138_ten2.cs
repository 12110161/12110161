namespace _12110161_Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ten2 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TheLoais",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        TenTheLoai = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            AddColumn("dbo.Posts", "TheLoaiID", c => c.Int(nullable: false));
            AddForeignKey("dbo.Posts", "TheLoaiID", "dbo.TheLoais", "ID", cascadeDelete: true);
            CreateIndex("dbo.Posts", "TheLoaiID");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Posts", new[] { "TheLoaiID" });
            DropForeignKey("dbo.Posts", "TheLoaiID", "dbo.TheLoais");
            DropColumn("dbo.Posts", "TheLoaiID");
            DropTable("dbo.TheLoais");
        }
    }
}
