﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using _12110161_Web.Models;

namespace _12110161_Web.Controllers
{
    [Authorize]
    public class PostController : Controller
    {
        private Web_12110161 db = new Web_12110161();

        //
        // GET: /Post/
        [AllowAnonymous]
        public ActionResult Index()
        {
            var posts = db.Posts.Include(p => p.UserProfile).Include(p => p.TheLoai);
            return View(posts.ToList());
        }

        //
        // GET: /Post/Details/5
        [AllowAnonymous]
        public ActionResult Details(int id = 0)
        {
            Post post = db.Posts.Find(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            ViewData["idpost"] = id;
            return View(post);
        }

        //
        // GET: /Post/Create

        public ActionResult Create()
        {
            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName");
            ViewBag.TheLoaiID = new SelectList(db.TheLoais, "ID", "TenTheLoai");
            return View();
        }

        //
        // POST: /Post/Create

        [HttpPost]
        public ActionResult Create(Post post,string Content)
        {
            if (ModelState.IsValid)
            {
                post.DayCreate = DateTime.Now;

                int userid = db.UserProfiles.Select(x => new { x.UserId, x.UserName })
                    .Where(y => y.UserName == User.Identity.Name).Single().UserId;
                post.UserProfileUserId = userid;

                
                // Tạo list tag cho Post
                List<Tag> Tags = new List<Tag>();
                // Tách các tag bằng dấu phẩy
                string[] TagContent = Content.Split(',');
                // lặp các tag vừa tách
                foreach (string item in TagContent)
                {
                    // Tìm xem các tag có hay chưa
                    Tag tagExits = null;
                    var ListTag = db.Tags.Where(y => y.Content.Equals(item));
                    if (ListTag.Count() > 0)
                    {

                        tagExits = ListTag.First();
                        tagExits.Posts.Add(post);
                    }
                    else
                    {
                        tagExits = new Tag();
                        tagExits.Content = item;
                        tagExits.Posts = new List<Post>();
                        tagExits.Posts.Add(post);
                    }

                    Tags.Add(tagExits);
                }
                // gán list tag vào post
                post.Tags = Tags;

                db.Posts.Add(post);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName", post.UserProfileUserId);
            ViewBag.TheLoaiID = new SelectList(db.TheLoais, "ID", "TenTheLoai", post.TheLoaiID);
            return View(post);
        }

        //
        // GET: /Post/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Post post = db.Posts.Find(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName", post.UserProfileUserId);
            ViewBag.TheLoaiID = new SelectList(db.TheLoais, "ID", "TenTheLoai", post.TheLoaiID);
            return View(post);
        }

        //
        // POST: /Post/Edit/5

        [HttpPost]
        public ActionResult Edit(Post post,string Content)
        {
            if (ModelState.IsValid)
            {
                post.DayCreate = DateTime.Now;
                int userid = db.UserProfiles.Select(x => new { x.UserId, x.UserName })
                    .Where(y => y.UserName == User.Identity.Name).Single().UserId;
                post.UserProfileUserId = userid;

                // Tạo list tag cho Post
                List<Tag> Tags = new List<Tag>();
                // Tách các tag bằng dấu phẩy
                string[] TagContent = Content.Split(',');
                // lặp các tag vừa tách
                foreach (string item in TagContent)
                {
                    // Tìm xem các tag có hay chưa
                    Tag tagExits = null;
                    var ListTag = db.Tags.Where(y => y.Content.Equals(item));
                    if (ListTag.Count() > 0)
                    {

                        tagExits = ListTag.First();
                        tagExits.Posts.Add(post);
                    }
                    else
                    {
                        tagExits = new Tag();
                        tagExits.Content = item;
                        tagExits.Posts = new List<Post>();
                        tagExits.Posts.Add(post);
                    }

                    Tags.Add(tagExits);
                }
                // gán list tag vào post
                post.Tags = Tags;

                db.Posts.Add(post);
                db.Entry(post).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName", post.UserProfileUserId);
            ViewBag.TheLoaiID = new SelectList(db.TheLoais, "ID", "TenTheLoai", post.TheLoaiID);
            return View(post);
        }

        //
        // GET: /Post/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Post post = db.Posts.Find(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            return View(post);
        }

        //
        // POST: /Post/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Post post = db.Posts.Find(id);
            db.Posts.Remove(post);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}