﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using _12110161_Web.Models;

namespace _12110161_Web.Controllers
{
    public class BangXepHangController : Controller
    {
        private Web_12110161 db = new Web_12110161();

        //
        // GET: /BangXepHang/

        public ActionResult Index()
        {
            var bangxephangs = db.BangXepHangs.Include(b => b.TheLoai);
            return View(bangxephangs.ToList());
        }

        //
        // GET: /BangXepHang/Details/5

        public ActionResult Details(int id = 0)
        {
            BangXepHang bangxephang = db.BangXepHangs.Find(id);
            if (bangxephang == null)
            {
                return HttpNotFound();
            }
            return View(bangxephang);
        }

        //
        // GET: /BangXepHang/Create

        public ActionResult Create()
        {
            ViewBag.TheLoaiID = new SelectList(db.TheLoais, "ID", "TenTheLoai");
            return View();
        }

        //
        // POST: /BangXepHang/Create

        [HttpPost]
        public ActionResult Create(BangXepHang bangxephang)
        {
            if (ModelState.IsValid)
            {
                db.BangXepHangs.Add(bangxephang);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.TheLoaiID = new SelectList(db.TheLoais, "ID", "TenTheLoai", bangxephang.TheLoaiID);
            return View(bangxephang);
        }

        //
        // GET: /BangXepHang/Edit/5

        public ActionResult Edit(int id = 0)
        {
            BangXepHang bangxephang = db.BangXepHangs.Find(id);
            if (bangxephang == null)
            {
                return HttpNotFound();
            }
            ViewBag.TheLoaiID = new SelectList(db.TheLoais, "ID", "TenTheLoai", bangxephang.TheLoaiID);
            return View(bangxephang);
        }

        //
        // POST: /BangXepHang/Edit/5

        [HttpPost]
        public ActionResult Edit(BangXepHang bangxephang)
        {
            if (ModelState.IsValid)
            {
                db.Entry(bangxephang).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.TheLoaiID = new SelectList(db.TheLoais, "ID", "TenTheLoai", bangxephang.TheLoaiID);
            return View(bangxephang);
        }

        //
        // GET: /BangXepHang/Delete/5

        public ActionResult Delete(int id = 0)
        {
            BangXepHang bangxephang = db.BangXepHangs.Find(id);
            if (bangxephang == null)
            {
                return HttpNotFound();
            }
            return View(bangxephang);
        }

        //
        // POST: /BangXepHang/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            BangXepHang bangxephang = db.BangXepHangs.Find(id);
            db.BangXepHangs.Remove(bangxephang);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}