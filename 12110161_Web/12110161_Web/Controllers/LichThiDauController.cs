﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using _12110161_Web.Models;

namespace _12110161_Web.Controllers
{
    public class LichThiDauController : Controller
    {
        private Web_12110161 db = new Web_12110161();

        //
        // GET: /LichThiDau/

        public ActionResult Index()
        {
            return View(db.LichThiDaus.ToList());
        }

        //
        // GET: /LichThiDau/Details/5

        public ActionResult Details(int id = 0)
        {
            LichThiDau lichthidau = db.LichThiDaus.Find(id);
            if (lichthidau == null)
            {
                return HttpNotFound();
            }
            return View(lichthidau);
        }

        //
        // GET: /LichThiDau/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /LichThiDau/Create

        [HttpPost]
        public ActionResult Create(LichThiDau lichthidau)
        {
            if (ModelState.IsValid)
            {
                db.LichThiDaus.Add(lichthidau);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(lichthidau);
        }

        //
        // GET: /LichThiDau/Edit/5

        public ActionResult Edit(int id = 0)
        {
            LichThiDau lichthidau = db.LichThiDaus.Find(id);
            if (lichthidau == null)
            {
                return HttpNotFound();
            }
            return View(lichthidau);
        }

        //
        // POST: /LichThiDau/Edit/5

        [HttpPost]
        public ActionResult Edit(LichThiDau lichthidau)
        {
            if (ModelState.IsValid)
            {
                db.Entry(lichthidau).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(lichthidau);
        }

        //
        // GET: /LichThiDau/Delete/5

        public ActionResult Delete(int id = 0)
        {
            LichThiDau lichthidau = db.LichThiDaus.Find(id);
            if (lichthidau == null)
            {
                return HttpNotFound();
            }
            return View(lichthidau);
        }

        //
        // POST: /LichThiDau/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            LichThiDau lichthidau = db.LichThiDaus.Find(id);
            db.LichThiDaus.Remove(lichthidau);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}